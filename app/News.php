<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class News extends Model
{
    protected $fillable = ['title', 'image', 'slug', 'category_id', 'description', 'content', 'publish_at', 'user_id'];

    public function category()
    {
    	return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'news_id', 'id');
    }

    public function publist_at_human()
    {
    	return Carbon::createFromFormat('Y-m-d H:i:s', $this->publish_at)->diffForHumans();
    }
}
