<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Carbon\Carbon;
use File;

use App\News;

use Sunra\PhpSimple\HtmlDomParser;
use GuzzleHttp\Client;


class CrawlerZing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:zing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lay tin tuc tu zing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // id category => url
        $urls = [
            1 => 'http://news.zing.vn/xa-hoi.html',
            2 => 'http://news.zing.vn/the-gioi.html',
            3 => 'http://news.zing.vn/thi-truong.html',
            4 => 'http://news.zing.vn/phap-luat.html',
            5 => 'http://news.zing.vn/the-gioi-sach.html',
            6 => 'http://news.zing.vn/the-thao.html',
            7 => 'http://news.zing.vn/cong-nghe.html',
            8 => 'http://news.zing.vn/oto-xe-may.html',
            9 => 'http://news.zing.vn/giai-tri.html',
            10 => 'http://news.zing.vn/am-nhac.html',
            11 => 'http://news.zing.vn/phim-anh.html',
            12 => 'http://news.zing.vn/thoi-trang.html',
            13 => 'http://news.zing.vn/song-tre.html',
            14 => 'http://news.zing.vn/giao-duc.html',
            15 => 'http://news.zing.vn/suc-khoe.html',
            16 => 'http://news.zing.vn/suc-khoe.html',
            17 => 'http://news.zing.vn/du-lich.html',
        ];
        
        // lap url tu the loai cua zing 
        foreach ($urls as $category_id => $url) {
            // lay cac bai viet theo url
            $this->comment('>>> Geting from :'.$url);
            $this->comment('>>> Will be insert to category_id :'.$category_id);

            foreach ($this->boc_tach_ds_bai_viet_danh_muc($url) as $article)
            {
                if(News::where('title', '=', $article['title'])->count()) {
                    $this->line('Exists in database : ' . $article['title']);
                    continue;
                }

                // tai hinh
                $article['image']   = $this->tai_hinh($article['cover'], Str::slug($article['title']));
                // lay noi dung chi tiet cua bai viet
                $article['content'] = $this->boc_tach_chi_tiet_bai_viet($article['link']);
                // lay hinh chi tiet bai viet
                $article['content'] = $this->xu_ly_hinh_chi_tiet_bai_viet($article['content']);


                News::create([
                    'title'         => $article['title'], 
                    'image'         => $article['image'], 
                    'slug'          => Str::slug($article['title']), 
                    'category_id'   => $category_id, 
                    'description'   => $article['intro'], 
                    'content'       => $article['content'], 
                    'user_id'       => 1,                
                    'publish_at'    => Carbon::now(),
                    'source'        => 'zing'
                ]);

                $this->info('Insert ok : '. $article['title']);
            }

            sleep(0);
        }

    }

    public function xu_ly_hinh_chi_tiet_bai_viet($noi_dung)
    {
        $html = HtmlDomParser::str_get_html($noi_dung);
        foreach($html->find('img') as $img)
        {

            $link_hinh = $this->tai_hinh($img->src, $ten_hinh = '');
            $noi_dung = str_replace($img->src, $link_hinh, $noi_dung);
        }
        return $noi_dung;
    }

    public function lay_html($url)
    {
        $client = new Client();
        $response = $client->request('GET', $url, ['decode_content' => 'gzip']);
        $html = $response->getBody();
        return (string) $html;
    }

    public function boc_tach_ds_bai_viet_danh_muc($url)
    {
        $articles = [];
        $html = HtmlDomParser::str_get_html($this->lay_html($url));
        $selector = $html->find('#category > div > section.cate_content > section.cate_content > article');
        if (empty($selector)) {
            $selector = $html->find('#category > div > section.cate_content > article');
        }
        foreach($selector as $article) {
            $item['link']     = "http://news.zing.vn".$article->find('header > h1 > a', 0)->href;
            $item['title']    = $article->find('header > h1 > a', 0)->plaintext;
            $item['cover']    = $article->find('div.cover', 0)->style;
            preg_match('/url\((.*)\)/', $item['cover'], $item['cover']);
            $item['cover'] = $item['cover'][1];
            $item['intro']    = $article->find('header > p.summary', 0)->plaintext;
            $articles[] = $item;
        }
        return $articles;
    }

    public function boc_tach_chi_tiet_bai_viet($url)
    {
        $html = HtmlDomParser::str_get_html($this->lay_html($url));
        $content = $html->find('section.main > div.the-article-body', 0)->innertext;
        return $content;
    }

    public function tai_hinh($url, $ten_hinh = '')
    {
        $thu_muc_chua = '/upload/images/zing/';
        
        if ( ! File::isDirectory(public_path() . $thu_muc_chua)) 
            File::makeDirectory(public_path() . $thu_muc_chua);
        
        $ten_hinh = $ten_hinh != '' ? $ten_hinh : md5(microtime());
        $duoi_file = explode('.', $url);
        $duoi_file = $duoi_file[count($duoi_file)-1];

        $file_path_absolute = public_path() . $thu_muc_chua . $ten_hinh . '.' .  $duoi_file;
        $file_path_relative = $thu_muc_chua . $ten_hinh . '.' .  $duoi_file;

        
        $img_binary = @file_get_contents($url);
        if ($img_binary == false) {
            $this->line('Failed to save img : '. $url);
            return false;
        }

        if (File::put($file_path_absolute, $img_binary));
            return $file_path_relative;

        return false;
    }
}
