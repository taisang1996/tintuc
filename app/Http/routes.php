<?php

Route::get('/test', function () {

    // echo "<pre>";
    // print_r(App\Category::select(['id', 'name'])->get()->toArray());

     Artisan::call('crawler:zing');

    // $news = App\News::all();
    // foreach ($news as $n)
    // {
    //     print_r($n->category);
    // }

    // $cate = App\Category::find(1);
    // dd($cate->news);


});

// Frontend
Route::group(['middleware' => ['web']], function () {

    Route::get('/', ['as' => 'home', 'uses' => 'FrontEndController@home']);
    Route::get('/about', ['as' => 'about', 'uses' => 'FrontEndController@about']);
    Route::get('/contact', ['as' => 'contact', 'uses' => 'FrontEndController@contact']);


    Route::get('/danh-muc/{slug}', ['as' => 'trang_danh_muc', 'uses' => 'FrontEndController@trang_tin_tuc_theo_danh_muc']);
    Route::get('/tin-tuc/{slug}', ['as' => 'trang_chi_tiet_tin_tuc', 'uses' => 'FrontEndController@trang_chi_tiet_tin_tuc']);

});


// dang nhap, dang xuat, dang ky
Route::group(['middleware' => ['web']], function () {
    Route::get('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('/login', ['as' => 'postLogin', 'uses' => 'Auth\AuthController@postLogin']);
    Route::get('/register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
    Route::post('/register', ['as' => 'postRegister', 'uses' => 'Auth\AuthController@postRegister']);
    Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
});


// Backend nhóm xử lý hệ thống
Route::group(['middleware' => ['web', 'auth']], function () {

    Route::group(['prefix' => 'backend'], function () {
        Route::get('/', function () {
            return redirect()->route('dashboard');
        });

        Route::get('/dashboard', ['as' => 'dashboard', function () {
            return view('backend.page.dashboard');
        }]);

        Route::resource('category', 'CategoryController');
        Route::resource('news', 'NewsController');
        Route::resource('comment', 'CommentController');
    });
});
