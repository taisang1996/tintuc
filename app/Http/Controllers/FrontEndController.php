<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\News;
use Carbon\Carbon;

class FrontEndController extends Controller
{

    /**
     * Trang chủ mặc định
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        $list_news_top = News::orderBy('publish_at', 'DESC')
            ->take(10)
            ->get();

        $list_category = Category::all();

        $list_news_recent = News::orderBy('publish_at', 'DESC')
            ->take(5)
            ->get();
        $list_news_top_view = News::orderBy('views', 'DESC')
            ->take(10)
            ->get();

        return view('frontend.home')
            ->with('list_news_top', $list_news_top)
            ->with('list_category', $list_category)
            ->with('list_news_recent', $list_news_recent)
            ->with('list_news_top_view', $list_news_top_view)
            ;
    }

    /**
     * Trang danh sách tin tức theo loại danh mục
     *
     * @param $slug
     * @return mixed
     */
    public function trang_tin_tuc_theo_danh_muc($slug)
    {

        // gioi han cua tin noi bat
        $limit_of_highlight = 7;

        //truy van danh muc hien tai
        $current_category = Category::where('slug', '=', $slug)->first();


        // ds tin tuc noi bat cua danh muc
        $list_news_highlight = $current_category->news()
            ->orderBy('publish_at', 'DESC')
            ->take($limit_of_highlight)
            ->get();


        // ds tin tuc con lai
        $list_news = $current_category->news()
            ->orderBy('publish_at', 'DESC')
            ->skip($limit_of_highlight)
            ->take(100)
            ->paginate(10);

        $list_news_recent = $current_category->news()
            ->orderBy('created_at', 'DESC')
            ->take(5)
            ->get();

        $list_news_top_view = $current_category->news()
            ->orderBy('views', 'DESC')
            ->orderBy('publish_at', 'DESC')
            ->take(5)
            ->get();

        return view('frontend.trang_tin_tuc_theo_danh_muc')
            ->with('current_category', $current_category)
            ->with('list_news_highlight', $list_news_highlight)
            ->with('list_news', $list_news)
            ->with('list_news_recent', $list_news_recent)
            ->with('list_news_top_view', $list_news_top_view);
    }


    /**
     * Hiển thị chi tiết tin tức
     * @param $slug
     * @return mixed
     */
    public function trang_chi_tiet_tin_tuc($slug)
    {
        $news = News::where('slug', $slug)->first();
        $news->increment('views', 1);

        $list_news_recent = News::where('category_id', $news->category_id)
            ->orderBy('created_at', 'DESC')
            ->take(5)
            ->get();

        $list_news_top_view = News::where('category_id', $news->category_id)
            ->orderBy('views', 'DESC')
            ->orderBy('publish_at', 'DESC')
            ->take(5)
            ->get();

        $list_news_similar = News::where('category_id', $news->category_id)
            ->orderBy('publish_at', 'DESC')
            ->skip(2)
            ->take(3)
            ->get();

        $list_news_next_previous = News::where('category_id', $news->category_id)
            ->whereBetween('id', [$news->id - 1, $news->id + 1])
            ->where('id', '!=', $news->id)
            ->get();


        return view('frontend.trang_chi_tiet_tin_tuc')
            ->with('news', $news)
            ->with('list_news_recent', $list_news_recent)
            ->with('list_news_top_view', $list_news_top_view)
            ->with('list_news_similar', $list_news_similar)
            ->with('list_news_next_previous', $list_news_next_previous);

    }

}
