<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str; 

class TableCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$cates = [
        	['name' => 'Thời sự'],
        	['name' => 'Thế giới'],
        	['name' => 'Kinh doanh'],
        	['name' => 'Pháp luật'],
        	['name' => 'Xuất bản'],
        	['name' => 'Thể thao'],
        	['name' => 'Công nghệ'],
        	['name' => 'Xe 360'],
        	['name' => 'Giải trí'],
        	['name' => 'Âm nhạc'],
        	['name' => 'Phim ảnh'],
        	['name' => 'Thời trang'],
        	['name' => 'Sống trẻ'],
        	['name' => 'Giáo dục'],
        	['name' => 'Sức khỏe'],
        	['name' => 'Du lịch'],
        ];

        foreach ($cates as $cate) {
        	$cate['slug'] = Str::slug($cate['name']);
        	App\Category::create($cate);
        }
       
    }
}
