<?php

use Illuminate\Database\Seeder;

class TableUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
        	'name' => 'Nguyễn Tấn Tài',
        	'email' => 'taisang1996@gmail.com',
        	'password' => Hash::make('taisang1996@gmail.com'),
        ]);

        $faker = Faker\Factory::create();
        foreach (range(1, 20) as $i) {
        	App\User::create([
	        	'name' => $faker->name,
	        	'email' => $faker->freeEmail,
	        	'password' => Hash::make('123456'),
	        ]);
        }
    }
}
