# Dự án Tin Tức - TOTB Team #

Đây là dự án tin tức của nhóm Think Outside The Box

Source code : https://bitbucket.org/taisang1996/tintuc.git

Công việc : https://trello.com/c/LKeOOjOk/8-project-tin-t-c

### Install


```
#!php

$ git clone https://bitbucket.org/taisang1996/tintuc.git

$ composer install
$ cp .env.example .env
$ php artisan migrate
$ php artisan db:seed
$ php artisan crawler:zing #cướp tin tức từ zing

* Deploy!!
```

Cần tuyển người tham gia project tin tức, vui lòng liên hệ cho em nhé. 

Yêu cầu :   

* tài khoản bitbucket.org (inbox tên tài khoản)
* trello.com (xem task)
* git cơ bản (commit, pull, push)
* laravel

### Database

```
#!php

/database/mo_hinh.png
```