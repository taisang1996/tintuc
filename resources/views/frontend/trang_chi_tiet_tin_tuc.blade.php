@extends('frontend.master')

@section('mainContent')
    <div class="content_bottom">
        {{--left--}}
        <div class="col-lg-8 col-md-8">
            <div class="content_bottom_left">
                <div class="single_page_area">
                    @include('frontend.blocks.breadcrumb')
                    @include('frontend.modules.chi_tiet_tin_tuc')
                </div>
            </div>

            <div class="post_pagination">
                <div class="prev">
                    <a class="angle_left" href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$list_news_next_previous[0]->slug])}}"><i class="fa fa-angle-double-left"></i></a>
                    <div class="pagincontent">
                        <span>Trước đó</span>
                        <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$list_news_next_previous[0]->slug])}}">{{$list_news_next_previous[0]->title}}</a>
                    </div>
                </div>
                <div class="next">
                    <div class="pagincontent">
                        <span>Kế tiếp</span>
                        <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$list_news_next_previous[1]->slug])}}">{{$list_news_next_previous[1]->title}}</a>
                    </div>
                    <a class="angle_right" href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$list_news_next_previous[1]->slug])}}"><i class="fa fa-angle-double-right"></i></a>
                </div>
            </div>

            <div class="share_post">
                <a class="facebook" href="#"><i class="fa fa-facebook"></i>Facebook</a>
                <a class="twitter" href="#"><i class="fa fa-twitter"></i>Twitter</a>
                <a class="googleplus" href="#"><i class="fa fa-google-plus"></i>Google+</a>
                <a class="linkedin" href="#"><i class="fa fa-linkedin"></i>LinkedIn</a>
                <a class="stumbleupon" href="#"><i class="fa fa-stumbleupon"></i>StumbleUpon</a>
                <a class="pinterest" href="#"><i class="fa fa-pinterest"></i>Pinterest</a>
            </div>

            @include('frontend.modules.tin_tuc_tuong_tu')
        </div>

        {{--right--}}
        <div class="col-lg-4 col-md-4">
            <!-- Tin tuc gan day -->
            @include('frontend.modules.tin_tuc_gan_day')
                    <!-- Xem nhieu va comment moi nhat theo tab -->
            @include('frontend.modules.xem_nhieu_va_comment_gan_day_theo_tab')
        </div>
    </div>
@stop