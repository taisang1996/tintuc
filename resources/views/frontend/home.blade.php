@extends('frontend.master')

@section('mainContent')

        <!-- start main content top -->
<div class="content_top">
    <div class="row">
        <!-- start content top latest slider -->
        <div class="col-lg-6 col-md-6 col-sm6">
            <div class="latest_slider">
                <!-- Set up your HTML -->
                <div class="slick_slider">
                    @foreach($list_news_top->take(3) as $key => $news)
                        <div class="single_iteam">
                            <img src="{{$news->image}}" alt="img">
                            <h2><a class="slider_tittle"
                                   href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}}
                                    t</a></h2>
                        </div>
                        <?php
                        $list_news_top->pull($key);
                        ?>
                    @endforeach

                </div>
            </div>
        </div> <!-- End content top latest slider -->

        <div class="col-lg-6 col-md-6 col-sm6">
            <div class="content_top_right">
                <ul class="featured_nav wow fadeInDown">
                    @foreach($list_news_top->take(4) as $key => $news)
                        <li>
                            <img src="{{$news->image}}" alt="img">
                            <div class="title_caption">
                                <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">
                                    {{$news->title}}
                                </a>
                            </div>
                        </li>
                        <?php $list_news_top->pull($key) ?>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</div><!-- End main content top -->

<!-- start main content Middle -->
<div class="content_middle">
    <div class="col-lg-3 col-md-3 col-sm-3">
        <div class="content_middle_leftbar">
            @include('frontend.modules.danh_muc_2_tin_tuc', ['category' => $list_category[5]])
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="content_middle_middle">
            <div class="slick_slider2">
                @foreach($list_news_top as $key => $news)
                    <div class="single_featured_slide">
                        <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}"><img src="{{$news->image}}"
                                                                                                  alt="img"></a>
                        <h2><a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}}</a>
                        </h2>
                        <p>{{$news->description}}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3">
        <div class="content_middle_rightbar">
            @include('frontend.modules.danh_muc_2_tin_tuc', ['category' => $list_category[6]])
        </div>
    </div>
</div><!-- End main content middle -->

<!-- start main content bottom -->
<div class="content_bottom">
    <div class="col-lg-8 col-md-8">
        <!-- start content bottom left -->
        <div class="content_bottom_left">

            <!-- danh_muc_2_cot_3_tin_tuc -->
            @include('frontend.modules.danh_muc_2_cot_3_tin_tuc', ['category' => $list_category[0]])
                    <!-- End danh_muc_2_cot_3_tin_tuc -->

            <!-- start games & fashion category -->
            <div class="games_fashion_area">
                <!-- danh_muc_1_cot_2_tin_tuc -->
                @include('frontend.modules.danh_muc_1_cot_2_tin_tuc', ['category' => $list_category[4]])
                        <!-- End danh_muc_1_cot_2_tin_tuc -->

                <!-- danh_muc_1_cot_2_tin_tuc -->
                @include('frontend.modules.danh_muc_1_cot_2_tin_tuc', ['category' => $list_category[7], 'right' => true])
                        <!-- End danh_muc_1_cot_2_tin_tuc -->

            </div><!-- End games & fashion category -->


            <!-- danh_muc_2_cot_3_tin_tuc -->
            @include('frontend.modules.danh_muc_2_cot_3_tin_tuc', ['category' => $list_category[1]])
                    <!-- End danh_muc_2_cot_3_tin_tuc -->

        </div><!--End content_bottom_left-->
    </div>
    <!-- start content bottom right -->
    <div class="col-lg-4 col-md-4">
        <div class="content_bottom_right">
            <!-- start tin_tuc_gan_day -->
            @include('frontend.modules.tin_tuc_gan_day')
                    <!-- End tin_tuc_gan_day -->
            <!-- start xem_nhieu_va_comment_gan_day_theo_tab -->
            @include('frontend.modules.xem_nhieu_va_comment_gan_day_theo_tab')
                    <!-- End xem_nhieu_va_comment_gan_day_theo_tab -->

        </div>
    </div>
    <!-- start content bottom right -->
</div><!-- end main content bottom -->

@stop