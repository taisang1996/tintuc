<div class="single_bottom_rightbar">
    <ul role="tablist" class="nav nav-tabs custom-tabs">
        <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#mostPopular">Xem
                nhiều</a></li>
        <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="messages" href="#recentComent">Bình luận
                mới</a></li>
    </ul>
    <div class="tab-content">
        <div id="mostPopular" class="tab-pane fade in active" role="tabpanel">
            <ul class="small_catg popular_catg wow fadeInDown animated"
                style="visibility: visible; animation-name: fadeInDown;">
                @foreach($list_news_top_view as $news)
                    <li>
                        <div class="media wow fadeInDown animated"
                             style="visibility: visible; animation-name: fadeInDown;">
                            <a class="media-left" href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">
                                <img src="{{$news->image}}" alt="img">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"><a
                                            href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{str_limit($news->title, 50)}}</a>
                                </h4>
                                <p>{{str_limit($news->description, 100)}}</p>
                            </div>
                        </div>
                    </li>
                @endforeach

            </ul>
        </div>
        <div id="recentComent" class="tab-pane fade" role="tabpanel">
            <ul class="small_catg popular_catg">
                <li>
                    <div class="media wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                        <a class="media-left" href="#">
                            <img src="img/devhill/112x112.jpg" alt="img">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="#">Duis condimentum nunc pretium lobortis </a></h4>
                            <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet
                                nulla nisl quis mauris. Suspendisse a pharetra </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                        <a class="media-left" href="#">
                            <img src="img/devhill/112x112.jpg" alt="img">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="#">Duis condimentum nunc pretium lobortis </a></h4>
                            <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet
                                nulla nisl quis mauris. Suspendisse a pharetra </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                        <a class="media-left" href="#">
                            <img src="img/devhill/112x112.jpg" alt="img">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="#">Duis condimentum nunc pretium lobortis </a></h4>
                            <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet
                                nulla nisl quis mauris. Suspendisse a pharetra </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>