<div class="similar_post">
    <h2>Similar Post You May Like <i class="fa fa-thumbs-o-up"></i></h2>
    <ul class="small_catg similar_nav wow fadeInDown animated animated"
        style="visibility: visible; animation-name: fadeInDown;">
        @foreach ($list_news_similar as $news)
        <li>
            <div class="media wow fadeInDown animated animated"
                 style="visibility: visible; animation-name: fadeInDown;">
                <a class="media-left related-img" href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">
                    <img src="{{$news->image}}" alt="img">
                </a>
                <div class="media-body">
                    <h4 class="media-heading"><a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}}</a></h4>
                    <p>{{str_limit($news->description)}}</p>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>