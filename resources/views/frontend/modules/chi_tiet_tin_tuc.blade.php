<h2 class="post_titile">{{$news->title}}</h2>
<div class="single_page_content">
    <div class="post_commentbox">
        <a href="#"><i class="fa fa-user"></i>{{$news->user->name}}</a>
        <span><i class="fa fa-calendar"></i>{{$news->publist_at_human()}}</span>
        <a href="{{route('trang_danh_muc', ['slug' => $news->category->slug])}}"><i class="fa fa-tags"></i>{{$news->category->name}}</a>
    </div>

    {!! $news->content !!}
</div>