<div class="content_bottom_left">

    <!-- start business category -->
    <div class="single_category wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">

        <div class="archive_style_1">
            <h2>
                <span class="bold_line"><span></span></span>
                <span class="solid_line"></span>
                <span class="title_text">Tin tức mới nhất</span>
            </h2>
            <?php $i = 0 ?>
            @foreach($list_news as $news)
                @if($i % 2 == 0)
                    <div class="clearfix"></div>
                @endif
                <div class="business_category_left wow fadeInDown animated"
                     style="visibility: visible; animation-name: fadeInDown;">
                    <ul class="fashion_catgnav">
                        <li>
                            <div class="catgimg2_container">
                                <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}"><img alt="img"
                                                                                                          src="{{$news->image}}"></a>
                            </div>
                            <h2 class="catg_titile"><a
                                        href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}}</a>
                            </h2>
                            <div class="comments_box">
                                <span class="meta_date">{{$news->publist_at_human()}}</span>
                                <span class="meta_comment"><a
                                            href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}#comments">No
                                        Comments</a></span>
                                <span class="meta_more"><a
                                            href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">Xem
                                        thêm...</a></span>
                            </div>
                            <p>{{$news->description}}.</p>
                        </li>

                    </ul>
                </div>
                <?php $i++ ?>
            @endforeach


        </div>


    </div>
    <!-- End business category -->

</div>

<div class="pagination_area">
    <nav>
        {!! $list_news->links() !!}
    </nav>
</div>