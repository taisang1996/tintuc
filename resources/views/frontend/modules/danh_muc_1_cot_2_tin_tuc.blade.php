<div class="games_category" style="float: {{ isset($right) ? 'right' : '' }}">
    <div class="single_category">
        <h2>
            <span class="bold_line"><span></span></span>
            <span class="solid_line"></span>
            <a class="title_text" href="category-archive.html">{{$category->name}}</a>
        </h2>
        <?php $news = $category->news->first() ?>
        <ul class="fashion_catgnav wow fadeInDown">
            <li>
                <div class="catgimg2_container">
                    <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}"><img alt="img"
                                                                                              src="{{$news->image}}"></a>
                </div>
                <h2 class="catg_titile"><a
                            href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}}</a></h2>
                <div class="comments_box">
                    <span class="meta_date">{{$news->publist_at_human()}}</span>
                    <span class="meta_comment"><a href="#">No Comments</a></span>
                    <span class="meta_more"><a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">Read
                            More...</a></span>
                </div>
                <p>{{str_limit($news->description)}}</p>
            </li>
        </ul>
        <ul class="small_catg wow fadeInDown">
            @foreach ($category->news()->skip(1)->take(2)->get() as $news)
                <li>
                    <div class="media">
                        <a class="media-left" href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">
                            <img src="{{$news->image}}" alt="img">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading"><a
                                        href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}} </a>
                            </h4>
                            <div class="comments_box">
                                <span class="meta_date">{{$news->publist_at_human()}}</span>
                                <span class="meta_comment"><a href="#">No Comments</a></span>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>