<div class="single_category wow fadeInDown">
    <h2>
        <span class="bold_line"><span></span></span>
        <span class="solid_line"></span>
        <a href="category-archive.html" class="title_text">{{$category->name}}</a>
    </h2>
    <ul class="catg1_nav">
        @foreach ($category->news->take(2) as $news)
            <li>
                <div class="catgimg_container">
                    <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">
                        <img alt="img" src="{{$news->image}}" class="catg1_img">
                    </a>
                </div>
                <h3 class="post_titile"><a
                            href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}}
                        {{$news->title}}s</a>
                </h3>
            </li>
        @endforeach
    </ul>
</div>