<div class="single_bottom_rightbar">
    <h2>Tin tức gần đây</h2>
    <ul class="small_catg popular_catg wow fadeInDown animated"
        style="visibility: visible; animation-name: fadeInDown;">
        @foreach($list_news_recent as $news)
        <li>
            <div class="media wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}" class="media-left">
                    <img alt="img" src="{{$news->image}}">
                </a>
                <div class="media-body">
                    <h4 class="media-heading"><a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{str_limit($news->title, 50)}}</a></h4>
                    <p>{{str_limit($news->description, 100)}}</p>
                </div>
            </div>
        </li>
        @endforeach

    </ul>
</div>