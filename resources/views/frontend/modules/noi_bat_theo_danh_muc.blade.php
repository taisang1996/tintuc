<div class="content_middle">
    <div class="col-lg-3 col-md-3 col-sm-3">
        <div class="content_middle_leftbar">
            <div class="single_category wow fadeInDown animated"
                 style="visibility: visible; animation-name: fadeInDown;">
                <ul class="catg1_nav">
                    @foreach($list_news_highlight->take(2) as $key => $news)
                        <li>
                            <div class="catgimg_container">
                                <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}" class="catg1_img">
                                    <img alt="img" src="{{$news->image}}">
                                </a>
                            </div>
                            <h3 class="post_titile"><a
                                        href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}}</a>
                            </h3>
                        </li>
                        <?php $list_news_highlight->pull($key) ?>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="content_middle_middle">
            <div class="slick_slider2 slick-initialized slick-slider">
                <div class="slick-list draggable" tabindex="0">
                    <div class="slick-track" style="opacity: 1; width: 1605px;">
                        <?php $i = 0 ?>
                        @foreach($list_news_highlight->take(3) as $key => $news)
                            <?php
                            $active = ($i == 0) ? 'slick-active' : ''
                            ?>
                            <div class="single_featured_slide slick-slide {{$active}}" index="{{$i}}"
                                 style="width: 535px; position: relative; left: 0px; top: 0px; z-index: 900; opacity: 1;">
                                <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}"><img
                                            src="{{$news->image}}" alt="img"></a>
                                <h2>
                                    <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}}</a>
                                </h2>
                                <p>{{$news->description}}</p>
                            </div>

                            <?php
                            $list_news_highlight->pull($key);
                            ?>
                        @endforeach
                    </div>
                </div>


                <button type="button" data-role="none" class="slick-prev" style="display: block;">Previous</button>
                <button type="button" data-role="none" class="slick-next" style="display: block;">Next</button>
                <ul class="slick-dots" style="display: block;">
                    <?php $i = 1 ?>


                    @for($i=1; $i<=3; $i++)
                        <?php
                        $active = ($i == 1) ? 'slick-active' : ''
                        ?>
                        <li class="{{$active}}">
                            <button type="button" data-role="none">{{$i}}</button>
                        </li>
                    @endfor
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3">
        <div class="content_middle_rightbar">
            <div class="single_category wow fadeInDown animated"
                 style="visibility: visible; animation-name: fadeInDown;">
                <ul class="catg1_nav">
                    @foreach($list_news_highlight->take(2) as $i=>$news)
                        <?php $list_news_highlight->pull($i) ?>
                        <li>
                            <div class="catgimg_container">
                                <a href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}" class="catg1_img">
                                    <img alt="img" src="{{$news->image}}">
                                </a>
                            </div>
                            <h3 class="post_titile"><a
                                        href="{{route('trang_chi_tiet_tin_tuc', ['slug'=>$news->slug])}}">{{$news->title}}</a>
                            </h3>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>