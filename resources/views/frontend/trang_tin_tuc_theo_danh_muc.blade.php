@extends('frontend.master')

@section('mainContent')
    <div class="content_top">
        @if(count($list_news_highlight))
            @include('frontend.modules.noi_bat_theo_danh_muc')
        @endif
    </div>
    <div class="content_bottom">
        <!-- Danh sach tin tuc moi nhat theo danh muc -->
        <div class="col-md-8 col-lg-8">
            @include('frontend.modules.danh_sach_tin_tuc_theo_danh_muc')
        </div>


        <div class="col-lg-4 col-md-4">
            <!-- Tin tuc gan day -->
            @include('frontend.modules.tin_tuc_gan_day')
                    <!-- Xem nhieu va comment moi nhat theo tab -->
            @include('frontend.modules.xem_nhieu_va_comment_gan_day_theo_tab')
        </div>
    </div>
@stop