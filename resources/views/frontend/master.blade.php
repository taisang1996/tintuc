<!DOCTYPE html>
<html lang="en">
<head>
    @section('head')
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@section('title', 'TOTB Team')@show</title>

        @section('css')
                <!-- Bootstrap -->
        <link href="{{url('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- for fontawesome icon css file -->
        <link href="{{url('frontend/css/font-awesome.min.css')}}" rel="stylesheet">
        <!-- for content animate css file -->
        <link rel="stylesheet" href="{{url('frontend/css/animate.css')}}">
        <!-- google fonts  -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'> -->
        <link href='https://fonts.googleapis.com/css?family=Roboto&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
        <!-- slick slider css file -->
        <link href="{{url('frontend/css/slick.css')}}" rel="stylesheet">
        <!-- <link href="css/theme-red.css')}}" rel="stylesheet"> -->
        <link href="{{url('frontend/css/theme.css')}}" rel="stylesheet">
        <!-- main site css file -->
        <link href="{{url('frontend/css/style.css')}}" rel="stylesheet">
        @show

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script type="text/javascript">
            url = '{{url('/')}}';
        </script>
    @show
</head>
<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- End Preloader -->

<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>

<div class="container">
    @include('frontend.blocks.header')

    @include('frontend.blocks.nav')

            <!-- start site main content -->
    <section id="mainContent">
        @section('mainContent')
            mainContent
        @show
    </section><!-- End site main content -->
</div> <!-- /.container -->

@include('frontend.blocks.footer')


@section('js')
    <!-- jQuery google CDN Library -->
    <script src="{{url('frontend/js/jquery.min.js')}}"></script>
    <!-- For content animatin  -->
    <script src="{{url('frontend/js/wow.min.js')}}"></script>
    <!-- bootstrap js file -->
    <script src="{{url('frontend/js/bootstrap.min.js')}}"></script>
    <!-- slick slider js file -->
    <script src="{{url('frontend/js/slick.min.js')}}"></script>
    <!-- custom js file include -->
    <script src="{{url('frontend/js/custom.js')}}"></script>
@show


</body>
</html>