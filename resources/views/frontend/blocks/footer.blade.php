<footer id="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="single_footer_top wow fadeInLeft">
                        <h2>Flicker Images</h2>
                        <ul class="flicker_nav">
                            <li>
                                <a href="#"><img src="{{url('frontend/img/devhill/75x75.jpg')}}" alt="img"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('frontend/img/devhill/75x75.jpg')}}" alt="img"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('frontend/img/devhill/75x75.jpg')}}" alt="img"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('frontend/img/devhill/75x75.jpg')}}" alt="img"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('frontend/img/devhill/75x75.jpg')}}" alt="img"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('frontend/img/devhill/75x75.jpg')}}" alt="img"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('frontend/img/devhill/75x75.jpg')}}" alt="img"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('frontend/img/devhill/75x75.jpg')}}" alt="img"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="single_footer_top wow fadeInDown">
                        <h2>Labels</h2>
                        <ul class="labels_nav">
                            @foreach (App\Category::all() as $cate)
                                <?php
                                if (isset($current_category)) {
                                    $active = ($current_category->id == $cate->id) ? "active" : '';
                                } else if (isset($news)) {
                                    $active = ($news->category_id == $cate->id) ? "active" : '';
                                } else {
                                    $active = (request()->segment(2) == $cate->slug) ? "active" : '';
                                }
                                ?>
                                <li class=" {{$active}}"><a
                                            href="{{route('trang_danh_muc', ['slug' => $cate->slug])}}">{{$cate->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="single_footer_top wow fadeInRight">
                        <h2>About Us</h2>
                        <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed nec laoreet orci, eget
                            ullamcorper quam. Phasellus lorem neque, </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="footer_bottom_left">
                        <p>Copyright © 2014 <a href="index.html">Sora Red</a></p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="footer_bottom_right">
                        <p>Designed BY <a href="http://wpfreeware.com/">Wpfreeware</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>