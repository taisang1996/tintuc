<ol class="breadcrumb">
    <li><a href="{{url('/')}}">Trang chủ</a></li>
    <li><a href="{{route('trang_danh_muc', ['slug' => $news->category->slug])}}">{{$news->category->name}}</a></li>
    <li class="active">{{$news->title}}</li>
</ol>