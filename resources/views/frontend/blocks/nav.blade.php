<!-- Static navbar -->
<div id="navarea">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav custom_nav">
                    <li class="{{request()->segment(1)===null ? 'active' : ''}}"><a href="{{url('/')}}">Trang chủ </a>
                    </li>
                    @foreach (App\Category::all() as $cate)
                        <?php
                        if (request()->segment(2) == null) {
                            $active = '';
                        }
                        else if (isset($current_category)) {
                            $active = ($current_category->id == $cate->id) ? "active" : '';
                        } else if (isset($news)) {
                            $active = ($news->category_id == $cate->id) ? "active" : '';
                        } else {
                            $active = (request()->segment(2) == $cate->slug) ? "active" : '';
                        }
                        ?>
                        <li class=" {{$active}}"><a
                                    href="{{route('trang_danh_muc', ['slug' => $cate->slug])}}">{{$cate->name}}</a></li>
                    @endforeach
                </ul>
            </div><!--/.nav-collapse -->

        </div><!--/.container-fluid -->
    </nav>
</div>