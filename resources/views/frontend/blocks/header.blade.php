<!-- start header area -->
<header id="header">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- start header top -->
            <div class="header_top">
                <div class="header_top_left">
                    <ul class="top_nav">
                        <li><a href="{{url('/')}}">Trang chủ</a></li>
                        <li><a href="{{route('about')}}">Giới thiệu</a></li>
                        <li><a href="{{route('contact')}}">Liên hệ</a></li>
                        @if(Auth::check())
                            <li><a href="{{route('dashboard')}}">Trang Admin</a></li>
                            <li><a href="{{route('logout')}}">Thoát</a></li>
                        @else
                            <li><a href="{{route('login')}}">Đăng nhập</a></li>
                        @endif
                    </ul>
                </div>
                <div class="header_top_right">
                    <form class="search_form">
                        <input type="text" placeholder="Text to Search">
                        <input type="submit" value="">
                    </form>
                </div>
            </div><!-- End header top -->
            <!-- start header bottom -->
            <div class="header_bottom">
                <div class="header_bottom_left">
                    <!-- for img logo -->

                    <!-- <a class="logo" href="index.html">
                      <img src="img/logo.jpg" alt="logo">
                     </a>-->
                    <!-- for text logo -->
                    <a class="logo" href="{{url('/')}}">
                        TOTB<strong>Team</strong> <span>Nhóm thiết kế web chuyên nghiệp</span>
                    </a>

                </div>
                <div class="header_bottom_right">
                    <a href="http://wpfreeware.com"><img src="{{url('frontend/img/addbanner_728x90_V1.jpg')}}"
                                                         alt="img"></a>
                </div>
            </div><!-- End header bottom -->
        </div>
    </div>
</header><!-- End header area -->