@extends('backend.layout.layout')
@section('title', 'Category List')
@section('content')
	<div class="page-header">
		<h1>Category List </h1>	
	</div>

	<div class="row">

		<div class="col-sm-8">
			@include('backend.common.alert')
			@include('backend.common.error')
		</div>

	</div>

	<div class="row">
		<div class="col-sm-4">
			<a href="{{route('backend.category.create')}}"class="btn btn-primary ">
				<i class="icon-plus"></i> Add new
			</a>	
		</div>
	</div>
	
	<hr>
	
	<div class="row">
		<div class="col-sm-12">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>id</th>
					<th>name</th>
					<th>slug</th>
					<th>category_parent_id</th>
					<th>created_at</th>
					<th>updated_at</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			@foreach(App\Category::all() as $category) 
				<tr>
					<td>{{$category->id}}</td>
					<td>{{$category->name}}</td>
					<td>{{$category->slug}}</td>
					<td>{{$category->category_parent_id}}</td>
					<td>{{$category->created_at}}</td>
					<td>{{$category->updated_at}}</td>
					<td>
						<a href="{{route('backend.category.edit', ['id'=> $category->id])}}" class="btn btn-primary btn-xs" alt="Edit"><i class="icon-pencil"></i></a>
						<form action="{{route('backend.category.destroy', ['id'=> $category->id])}}" method="post" class="form-inline" role="form">
							{{csrf_field()}}
							{{method_field('DELETE')}}
							<button type="submit" class="btn btn-danger btn-xs" alt="Edit"><i class="icon-trash "></i></button>
						</form>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		
		</div>
	</div>

@stop