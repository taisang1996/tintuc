@extends('backend.layout.layout')
@section('title', 'Create Category')
@section('content')
	<div class="page-header">
		<h1>Create Category</h1>	
	</div>

	<div class="row">
		<div class="col-sm-8">
			@include('backend.common.alert')
			@include('backend.common.error')
		</div>
	</div>


	<hr>

	<div class="row">
		<div class="col-sm-8">
			<form action="{{route('backend.category.store')}}" method="POST" class="form-horizontal" role="form">
				{{ method_field('POST') }}
				{{ csrf_field() }}
				<div class="form-group">
					<label for="inputName" class="col-sm-2 control-label">Name:</label>
					<div class="col-sm-10">
						<input type="text" name="name" id="inputName" class="form-control" value="" required="required" title="">
					</div>
				</div>
				<div class="form-group">
					<label for="inputSlug" class="col-sm-2 control-label">Slug:</label>
					<div class="col-sm-10">
						<input type="text" name="slug" id="inputSlug" class="form-control" value="" title="">
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputCategory_parent_id" class="col-sm-2 control-label">Category_parent_id:</label>
					<div class="col-sm-10">
						<select name="category_parent_id" id="inputCategory_parent_id" class="form-control">
							<option value="0">-- Select One --</option>
							@foreach (App\Category::all() as $cate)
								@if(isset($category) && $cate->id == $category->id)
									<option value="{{$cate->id}}" selected>{{$cate->name}} --- đang chọn</option>
								@else
									<option value="{{$cate->id}}">{{$cate->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-10 col-sm-offset-2">
						<button type="submit" class="btn btn-primary">
							<i class="icon-check"></i> Thêm
						</button>
					</div>
				</div>
				
			</form>

		</div>
	</div> <!-- .row -->
	
	<hr>
	<div class="row">
		<div class="col-sm-4">
			<a href="{{route('backend.category.index')}}"class="">
				<i class="icon-circle-arrow-left"></i> Category list
			</a>	
		</div>
	</div>
@stop