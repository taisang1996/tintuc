<div class="form-group">
	<label for="category_id">Category</label>
	<select name="category_id" id="inputCategory_id" class="form-control">
		<option value="0">-- Select One --</option>
		@foreach (App\Category::all() as $cate)
			<option value="{{$cate->id}}">{{$cate->name}}</option>
		@endforeach
	</select>
</div>