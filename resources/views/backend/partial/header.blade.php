      <!--header start-->
      <header class="header white-bg">
          <div class="sidebar-toggle-box">
              <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div>
          </div>
          <!--logo start-->
          <a href="{{route('dashboard')}}" class="logo" >Flat<span>lab</span></a>
          <!--logo end-->
          <div class="nav notify-row" id="top_menu">
            @include('backend.partial.notification')
          </ul>
          </div>
          <div class="top-nav ">
            @include('backend.partial.topnav')
          </div>
      </header>
      <!--header end-->