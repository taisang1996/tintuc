              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a href="{{route('dashboard')}}">
                          <i class="icon-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="icon-laptop"></i>
                          <span>Category</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{route('backend.category.create')}}">Add new Category</a></li>
                          <li><a  href="{{route('backend.category.index')}}">List Category</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="icon-book"></i>
                          <span>News</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{route('backend.news.create')}}">Add new News</a></li>
                          <li><a  href="{{route('backend.news.index')}}">List News</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="icon-cogs"></i>
                          <span>Comment</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{route('backend.comment.index')}}">List comment</a></li>
                      </ul>
                  </li>
          

              </ul>
              <!-- sidebar menu end-->