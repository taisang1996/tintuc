@extends('backend.layout.layout')
@section('title', 'Create News')
@section('content')
	<div class="page-header">
		<h1>Create News</h1>	
	</div>

	<div class="row">
		<div class="col-sm-8">
			@include('backend.common.alert')
			@include('backend.common.error')
		</div>
	</div>

	<hr>


	<div class="row">
		<div class="col-sm-8">
			<p>
				<a href="{{route('backend.news.index')}}" class="pull-right"><i class="icon-arrow-left"></i>List News</a>
			</p>
			<form action="{{route('backend.news.store')}}" method="POST" role="form">
				{{csrf_field()}}
				{{method_field('POST')}}
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" name="title" id="inputTitle" class="form-control" value="" title="">
				</div>
				
				<div class="form-group">
					<label for="category_id">Category</label>
					<select name="category_id" id="inputCategory_id" class="form-control">
						<option value="0">-- Select One --</option>
						@foreach (App\Category::all() as $cate)
							<option value="{{$cate->id}}">{{$cate->name}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label for="slug">Slug</label>
					<input type="text" name="slug" id="inputSlug" class="form-control" value="" title="" disabled>
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<textarea name="description" id="inputDescription" class="form-control" rows="3"></textarea>
				</div>
				<div class="form-group">
					<label for="content">Content</label>
					<textarea name="content" id="inputContent" class="form-control" rows="50"></textarea>
				</div>
				<div class="form-group">
					<label for="publish_at">Publish at</label>
					<input type="date" name="publish_at" id="inputPublish_at" class="form-control" value="" title="">
				</div>

				<button type="submit" class="btn btn-success"><i class="icon-plus"></i> Add news</button>

			</form>
		</div>
	</div>
	
		
@stop

@section('js')
	@parent
	@include('backend.common.editor')
@stop