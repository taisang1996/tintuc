@extends('backend.layout.layout')
@section('title', 'Edit News # '.$news->id)
@section('content')
	<div class="page-header">
		<h1>Edit News # {{$news->id}}</h1>	
	</div>

	<div class="row">
		<div class="col-sm-8">
			@include('backend.common.alert')
			@include('backend.common.error')
		</div>
	</div>

	<hr>


	<div class="row">
		<div class="col-sm-8">
			<p>
				<a href="{{route('backend.news.index')}}" class="pull-right"><i class="icon-arrow-left"></i>List News</a>
			</p>
			<form action="{{route('backend.news.update', ['id' => $news->id])}}" method="POST" role="form">
				{{csrf_field()}}
				{{method_field('PUT')}}
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" name="title" id="inputTitle" class="form-control" value="{{$news->title}}" title="">
				</div>

				<div class="row">

					<div class="col-sm-6">
						<div class="form-group">
							<label for="category_id">Category</label>
							<select name="category_id" id="inputCategory_id" class="form-control">
								<option value="0">-- Select One --</option>
								@foreach (App\Category::all() as $cate)
									@if ($cate->id == $news->category_id)
										<option value="{{$cate->id}}" selected>{{$cate->name}} --- đang chọn</option>
									@endif
									<option value="{{$cate->id}}">{{$cate->name}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="slug">Slug</label>
							<input type="text" name="slug" id="inputSlug" class="form-control" value="{{$news->slug}}" title="" disabled>
						</div>


					</div>

					<div class="col-sm-6">
						@if ($news->image)
							<a href="#" class="thumbnail">
								<img src="{{url($news->image)}}" alt="{{$news->title}}" >
							</a>
						@else
							<span class="label label-warning">No image</span>
						@endif
					</div>
				</div>

				<div class="form-group">
					<label for="inputImage">Image</label>
					<input type="text" name="image" id="inputImage" class="form-control" value="{{$news->image}}" title="">
				</div>

				<div class="form-group">
					<label for="description">Description</label>
					<textarea name="description" id="inputDescription" class="form-control" rows="3">{{$news->description}}</textarea>
				</div>
				<div class="form-group">
					<label for="content">Content</label>
					<textarea name="content" id="inputContent" class="form-control" rows="50">{{$news->content}}</textarea>
				</div>
				<div class="form-group">
					<label for="publish_at">Publish at</label>
					{{-- <input type="text" name="publish_at" id="inputPublish_at" class="form_datetime form-control" value="{{$news->publish_at}}" title=""> --}}
					<div class="row">
						<div class="col-sm-4">
							<div class="input-group date form_datetime-component">
								<input type="text" name="publish_at" class="form-control" value="{{$news->publish_at}}">
								<span class="input-group-btn">
									<button type="button" class="btn btn-danger date-set"><i class="icon-calendar"></i></button>
								</span>
							</div>
						</div>
					</div>

				</div>

				<div class="form-group">
					<label for="created_at">Created at</label>
					<input type="datetime" name="created_at" id="inputcreated_at" class="form-control" value="{{$news->created_at}}" title="" disabled>
				</div>
				<div class="form-group">
					<label for="updated_at">Publish at</label>
					<input type="datetime" name="updated_at" id="inputupdated_at" class="form-control" value="{{$news->updated_at}}" title="" disabled>
				</div>

				<button type="submit" class="btn btn-success"><i class="icon-save"></i> Save</button>

			</form>
		</div>
	</div>


@stop

@section('css-append')
	@parent
	<link rel="stylesheet" href="{{url('assets/bootstrap-datetimepicker/css/datetimepicker.css')}}">
@stop

@section('js-append')
	@parent
	@include('backend.common.editor')
	<script type="text/javascript" src="{{url('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
	<script>
		$(document).ready(function() {
		    CKFinder.setupCKEditor();
		    CKEDITOR.replace( 'inputContent', {
		        filebrowserBrowseUrl: base_url+'/assets/ckfinder/ckfinder.html',
		        filebrowserUploadUrl: base_url+'/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
		    });
		    CKEDITOR.replace( 'inputContent' );

			$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
			$(".form_datetime-component").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
		});
	</script>
@stop