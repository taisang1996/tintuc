@extends('backend.layout.layout')
@section('title', 'List News')
@section('css-append')
	@parent
	<link rel="stylesheet" href="{{url('assets/advanced-datatable/media/css/demo_table.css')}}">
	<style>
		table img {
			width: 100px;
		}
	</style>
@stop
@section('js-append')
	@parent
	<script src="{{url('assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
@stop
@section('content')
	<div class="page-header">
		<h1>List News</h1>	
	</div>

	<div class="row">
		<div class="col-sm-8">
			@include('backend.common.alert')
			@include('backend.common.error')
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<a href="{{route('backend.news.create')}}"class="btn btn-primary ">
				<i class="icon-plus"></i> Add new
			</a>	
		</div>
	</div>
	
	<hr>

	<div class="row">
		<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>id</th>
						<th>Hình ảnh</th>
						<th>Tiêu đề</th>
						<th>Danh mục</th>
						<th>Người đăng</th>
						<th>Xuất bản lúc</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				@foreach($list_news as $news)
					<tr>
						<td>{{$news->id}}</td>
						<td>
							@if ($news->image)
								<img src="{{url($news->image)}}" alt="{{$news->title}}" class="img-responsive" >
							@else
								<span class="label label-warning">No image</span>
							@endif
						</td>
						<td>{{$news->title}}</td>
						<td>
							{{$news->category->name or 'N/A'}}
						</td>
						<td>{{$news->user->name or 'N/A'}}</td>
						<td>{{$news->publish_at}}</td>
						<td>
						<a href="{{route('backend.news.edit', ['id'=> $news->id])}}" class="btn btn-primary btn-xs" alt="Edit"><i class="icon-pencil"></i></a>
						<form action="{{route('backend.news.destroy', ['id'=> $news->id])}}" method="post" class="form-inline" role="form">
							{{csrf_field()}}
							{{method_field('DELETE')}}
							<button type="submit" class="btn btn-danger btn-xs" alt="Edit"><i class="icon-trash "></i></button>
						</form>
					</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		{!! $list_news->links() !!}
		</div>
	</div>

@stop