<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="{{url('img/favicon.png')}}">

    <title>@yield('title', 'Quản lý tin tức')</title>
    @section('css')
    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('css/bootstrap-reset.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    @section('css-append')
    @show
    <!-- Custom styles for this template -->
    <link href="{{url('css/style.css')}}" rel="stylesheet">
    <link href="{{url('css/style-responsive.css')}}" rel="stylesheet" />
    @show
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{url('js/html5shiv.js')}}"></script>
    <script src="{{url('js/respond.min.js')}}"></script>
    <![endif]-->

    
    <script>
      base_url = '{!!url('/')!!}';
    </script>
  </head>

  <body>
@section('body')
  <section id="container" class="">

        @section('header')
            @include('backend.partial.header')
        @show

        @section('sidebar')
          <!--sidebar start-->
          <aside>
              <div id="sidebar"  class="nav-collapse ">
                @include('backend.partial.sidebar')
              </div>
          </aside>
          <!--sidebar end-->
        @show

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
                @section('content')
                Page content goes here
                @show
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      
       @section('footer')
        @include('backend.partial.footer')
       @show

  </section>

    @section('js')
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="{{url('js/jquery.js')}}"></script>
        <script src="{{url('js/bootstrap.min.js')}}"></script>
        <script src="{{url('js/jquery.dcjqaccordion.2.7.js')}}"></script>
        <script src="{{url('js/jquery.scrollTo.min.js')}}"></script>
        <script src="{{url('js/jquery.nicescroll.js')}}"></script>
        <script src="{{url('js/respond.min.js')}}" ></script>
        @section('js-append')
        @show
        <!--common script for all pages-->
        <script src="{{url('js/common-scripts.js')}}"></script>
    @show

@show

  </body>
</html>
