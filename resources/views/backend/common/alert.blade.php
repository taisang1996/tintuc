@if(Session::has('success'))
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Thành công!</strong> 
		@if(is_array(Session::get('success')))
			<ul>
			@foreach(Session::get('success') as $message)
				<li>{{$message}}</li>
			@endforeach
			</ul>
		@else
			{{Session::get('success')}}
		@endif
	</div>
@endif

@if(Session::has('danger'))
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Lỗi nguy hiểm!</strong> 
		@if(is_array(Session::get('danger')))
			<ul>
			@foreach(Session::get('danger') as $message)
				<li>{{$message}}</li>
			@endforeach
			</ul>
		@else
			{{Session::get('danger')}}
		@endif
	</div>
@endif

@if(Session::has('warning'))
	<div class="alert alert-warning">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Cảnh báo!</strong> 
		@if(is_array(Session::get('warning')))
			<ul>
			@foreach(Session::get('warning') as $message)
				<li>{{$message}}</li>
			@endforeach
			</ul>
		@else
			{{Session::get('warning')}}
		@endif
	</div>
@endif

@if(Session::has('info'))
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Thông tin!</strong> 
		@if(is_array(Session::get('info')))
			<ul>
			@foreach(Session::get('info') as $message)
				<li>{{$message}}</li>
			@endforeach
			</ul>
		@else
			{{Session::get('info')}}
		@endif
	</div>
@endif

