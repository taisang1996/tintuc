@extends('backend.layout.layout')
@section('content')
	<div class="page-header">
	  <h1>Dashboard <small></small></h1>

<div class="row state-overview">
	<div class="col-lg-3 col-sm-6">
	  <section class="panel">
	      <div class="symbol terques">
	          <i class="icon-user"></i>
	      </div>
	      <div class="value">
	          <h1 class="count">{{App\User::count()}}</h1>
	          <p>Users</p>
	      </div>
	  </section>
	</div>
	<div class="col-lg-3 col-sm-6">
	  <section class="panel">
	      <div class="symbol red">
	          <i class="icon-list-ul"></i>
	      </div>
	      <div class="value">
	          <h1 class=" count2">{{App\News::count()}}</h1>
	          <p>News</p>
	      </div>
	  </section>
	</div>
	<div class="col-lg-3 col-sm-6">
	  <section class="panel">
	      <div class="symbol yellow">
	          <i class="icon-comment"></i>
	      </div>
	      <div class="value">
	          <h1 class=" count3">{{App\Comment::count()}}</h1>
	          <p>Comments</p>
	      </div>
	  </section>
	</div>
	<div class="col-lg-3 col-sm-6">
	  <section class="panel">
	      <div class="symbol blue">
	          <i class="icon-bar-chart"></i>
	      </div>
	      <div class="value">
	          <h1 class=" count4">{{App\Category::count()}}</h1>
	          <p>Categories</p>
	      </div>
	  </section>
	</div>
	</div>
	</div>
@stop