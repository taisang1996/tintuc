
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="{{url('frontend/img/favicon.png')}}">

    <title>@yield('title', 'FlatLab Frontend | Home')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('frontend/css/theme.css')}}" rel="stylesheet">
    <link href="{{url('frontend/css/bootstrap-reset.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{url('frontend/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{url('frontend/css/flexslider.css')}}"/>
    <link href="{{url('frontend/assets/bxslider/jquery.bxslider.css')}}" rel="stylesheet" />
    <link href="{{url('frontend/assets/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet" />

    <link rel="stylesheet" href="{{url('frontend/assets/revolution_slider/css/rs-style.css')}}" media="screen">
    <link rel="stylesheet" href="{{url('frontend/assets/revolution_slider/rs-plugin/css/settings.css')}}" media="screen">

    <!-- Custom styles for this template -->
    <link href="{{url('frontend/css/style.css')}}" rel="stylesheet">
    <link href="{{url('frontend/css/style-responsive.css')}}" rel="stylesheet" />

    @section('css-append')
    @show

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="{{url('frontend/js/html5shiv.js')}}"></script>
      <script src="{{url('frontend/js/respond.min.js')}}"></script>
    <![endif]-->
  </head>

  <body>
	
	@section('header')
		@include('frontend.partial.header')
	@show

	@section('slider')
		@include('frontend.partial.slider')
	@show
  
  @section('breadcrumbs')
  @show

	@section('content')
    <!--container start-->
    <div class="container">
        <div class="row">
            <!--feature start-->
            <div class="text-center feature-head">
                <h1>welcome to flatlab</h1>
                <p>Professional html Template Perfect for Admin Dashboard</p>
            </div>
            <div class="col-lg-4 col-sm-4">
                <section>
                    <div class="f-box">
                        <i class=" icon-desktop"></i>
                        <h2>responsive design</h2>
                    </div>
                    <p class="f-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ablic jiener.</p>
                </section>
            </div>
            <div class="col-lg-4 col-sm-4">
                <section>
                    <div class="f-box active">
                        <i class=" icon-code"></i>
                        <h2>friendly code</h2>
                    </div>
                    <p class="f-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ablic jiener.</p>
                </section>
            </div>
            <div class="col-lg-4 col-sm-4">
                <section>
                    <div class="f-box">
                        <i class="icon-gears"></i>
                        <h2>fully customizable</h2>
                    </div>
                    <p class="f-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ablic jiener.</p>
                </section>
            </div>
            <!--feature end-->
        </div>
        <div class="row">
            <!--quote start-->
            <div class="quote">
                <div class="col-lg-9 col-sm-9">
                    <div class="quote-info">
                        <h1>developer friendly code</h1>
                        <p>Bundled with awesome features, UI resource unlimited colors, advanced theme options & much more!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3">
                    <a href="#" class="btn btn-danger purchase-btn">Purchase now</a>
                </div>
            </div>
            <!--quote end-->
        </div>
    </div>
     <!--container end-->
	@show

	@section('footer')
		@include('frontend.partial.footer')
	@show
		
    <!-- js placed at the end of the document so the pages load faster -->
    @section('js')
    <script src="{{url('frontend/js/jquery.js')}}"></script>
    <script src="{{url('frontend/js/jquery-1.8.3.min.js')}}"></script>
    <script src="{{url('frontend/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('frontend/js/hover-dropdown.js')}}"></script>
    <script defer src="{{url('frontend/js/jquery.flexslider.js')}}"></script>
    <script type="text/javascript" src="{{url('frontend/assets/bxslider/jquery.bxslider.js')}}"></script>

    <script type="text/javascript" src="{{url('frontend/js/jquery.parallax-1.1.3.js')}}"></script>

    <script src="{{url('frontend/js/jquery.easing.min.js')}}"></script>
    <script src="{{url('frontend/js/link-hover.js')}}"></script>

    <script src="{{url('frontend/assets/fancybox/source/jquery.fancybox.pack.js')}}"></script>

    <script type="text/javascript" src="{{url('frontend/assets/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
    <script type="text/javascript" src="{{url('frontend/assets/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

    <!--common script for all pages-->
    <script src="{{url('frontend/js/common-scripts.js')}}"></script>

    <script src="{{url('frontend/js/revulation-slide.js')}}"></script>


  <script>

      RevSlide.initRevolutionSlider();

      $(window).load(function() {
          $('[data-zlname = reverse-effect]').mateHover({
              position: 'y-reverse',
              overlayStyle: 'rolling',
              overlayBg: '#fff',
              overlayOpacity: 0.7,
              overlayEasing: 'easeOutCirc',
              rollingPosition: 'top',
              popupEasing: 'easeOutBack',
              popup2Easing: 'easeOutBack'
          });
      });

      $(window).load(function() {
          $('.flexslider').flexslider({
              animation: "slide",
              start: function(slider) {
                  $('body').removeClass('loading');
              }
          });
      });

      //    fancybox
      jQuery(".fancybox").fancybox();



  </script>
    @section('js-append')
    @show
	@show
  </body>
</html>
