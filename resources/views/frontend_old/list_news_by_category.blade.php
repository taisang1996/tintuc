@extends('frontend.layout')
@section('title', $category->name)
@section('slider')
@stop
@section('css-append')
<style>
	.box {
		margin-bottom: 2%;
	}
	.box img {
		width: 100%;
	}

</style>
@stop
@section('breadcrumbs')
	@include('frontend.partial.breadcrumbs')
@stop
@section('content')
<div class="container">

	@foreach ($list_news as $i => $news)
		@if($i%4==0)
			<div class="clearfix"> </div>
		@endif
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 box">
		  <div class="thumbnail">
			<a href="{{route('detail_news', $news->slug)}}" title="{{$news->title}}" >
		      <img src="{{$news->image == 0 ? url($news->image) : 'http://placeholdit.imgix.net/~text?txtsize=33&txt=No%20image&w=350&h=150'}}" alt="...">
		    </a>
		      <div class="caption">
		        <h3><a href="{{route('detail_news', $news->slug)}}" title="{{$news->title}}" >{{$news->title}}</a></h3>
		        <p>{{$news->description}} <br><span class="pull-right"><i class="icon-user"></i> {{$news->user->name}}</span></p>
		        <div class="clearfix"></div>
		        <p>
		        <a href="{{route('detail_news', $news->slug)}}" title="Xem {{$news->title}}" class="label label-primary" role="button"><i class="icon-eye-open"></i> Xem</a> 
		        <span class="label label-default" title="{{$news->publish_at}}"><i class="icon-time"></i> {{$news->publist_at_human()}}</span>
		        <span class="label label-default"><i class="icon-comment"></i> {{count($news->comments)}}</span>
		        
		        </p>
		      </div>
		    </div>
		</div>

	@endforeach
	
	<div class="clearfix"></div>
	<div class="row">
		<div class="text-center">
		{!! $list_news->links() !!}
		</div>
	</div>
</div>

@stop