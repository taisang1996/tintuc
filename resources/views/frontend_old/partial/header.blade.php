<!--header start-->
<header class="header-frontend">
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}">Flat<span>Lab</span></a>
            </div>
            <div class="navbar-collapse collapse ">
                <ul class="nav navbar-nav">
                @foreach (App\Category::where('status_display', '=', 1)->orderBy('order', 'ASC')->orderBy('id')->take(8)->get() as $cate)
                    @if($cate->slug == $category->slug)
                    <li class="active">
                    @else
                    <li>
                    @endif
                    <a href="{{route('category', ['slug' => $cate->slug])}}">
                        {{$cate->name}} 
                        @if (count($cate->news))
                            <sup>{{count($cate->news)}}</sup> 
                        @endif
                    </a></li>
                @endforeach
                    <li><input type="text" placeholder=" Search" class="form-control search"></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!--header end-->