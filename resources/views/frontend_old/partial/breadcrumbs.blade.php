<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-4">
                <h1>@yield('title')</h1>
            </div>
            <div class="col-lg-8 col-sm-8">
                <ol class="breadcrumb pull-right">
                    <li><a href="{{url('/')}}">Trang chủ</a></li>
                    @if(request()->segment(1) == 'news')
                        <li><a href="{{route('category', ['slug' => $news->category->slug])}}">{{$news->category->name}}</a></li>
                        <li class="active">{{$news->title}}</li>
                    @elseif(request()->segment(1) == 'category')
                        <li><a href="{{route('category', ['slug' => $category->slug])}}">{{$category->name}}</a></li>
                    @endif
                </ol>
            </div>
        </div>
    </div>
</div>