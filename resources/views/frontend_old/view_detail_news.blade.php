@extends('frontend.layout')
@section('title', $category->name)
@section('slider')
@stop
@section('breadcrumbs')
	@include('frontend.partial.breadcrumbs')
@stop
@section('css-append')
    <style>
    .blog-item h1, .blog-item h1 a {font-weight: bold;font-size: 36px;}
    .blog-item  {font-size: 16px; text-align: justify;}
    .blog-item p {line-height: 24px; margin-top: 10px;}
    .pCaption.caption { padding: 5px 0 8px; font-size: 80%;}
    </style>
@stop
@section('js-append')
<script>
$(function() {
    $('.blog-item img').addClass('img-responsive');
});
</script>
@stop
@section('content')
<div class="container">
<div class="row">
            <!--blog start-->
            <div class="col-lg-9">
                <div class="blog-item">
                    <div class="row">
                        <div class="col-lg-2 col-sm-2">
                            <div class="date-wrap">
                                <span class="date">{{date_format(date_create($news->publish_at), 'd')}}</span>
                                <span class="month">{{date_format(date_create($news->publish_at), 'M')}}</span>
                            </div>
                            <div class="comnt-wrap">
                                <span class="comnt-ico">
                                    <i class="icon-comments"></i>
                                </span>
                                <span class="value">{{count($news->comments)}}</span>
                            </div>
                            <div class="author text-right">
                                Đăng bởi <a href="#{{$news->user->id}}">{{$news->user->name}}</a>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <h1><a href="blog_detail.html">{{$news->title}}</a></h1>
                            {!! $news->content!!}
                            <div class="media">
                                <h3>Bình luận</h3>
                                <hr>
                                <a class="pull-left" href="javascript:;">
                                    <img class="media-object" src="{{url('img/avatar1.jpg')}}" alt="">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        Anjelina Joli <span>|</span>
                                        <span>12 July 2013, 10:20 </span>
                                    </h4>
                                    <p>
                                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
                                    </p>
                                    <a href="javascript:;">Reply</a>
                                    <hr>
                                    <!-- Nested media object -->
                                    <div class="media">
                                        <a class="pull-left" href="javascript:;">
                                            <img class="media-object" src="{{url('img/avatar2.jpg')}}" alt="">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                Tom Cruse <span>|</span>
                                                <span>12 July 2013, 10:30 </span>
                                            </h4>
                                            <p>
                                                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
                                            </p>
                                            <a href="javascript:;">Reply</a>
                                        </div>
                                    </div>
                                    <!--end media-->
                                    <hr>
                                    <div class="media">
                                        <a class="pull-left" href="javascript:;">
                                            <img class="media-object" src="{{url('frontend/img/avatar3.jpg')}}" alt="">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                Nicol Kidman <span>|</span>
                                                <span>12 July 2013, 10:40 </span>
                                            </h4>
                                            <p>
                                                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
                                            </p>
                                            <a href="javascript:;">Reply</a>
                                        </div>
                                    </div>
                                    <hr>
                                    <!--end media-->
                                </div>
                            </div>
                            <div class="media">
                                <a class="pull-left" href="javascript:;">
                                    <img class="media-object" src="{{url('img/avatar2.jpg')}}" alt="">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        Tom Cruse <span>|</span>
                                        <span>12 July 2013, 11:10 </span>
                                    </h4>
                                    <p>
                                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
                                    </p>
                                    <a href="javascript:;">Reply</a>
                                </div>
                            </div>
                            <div class="post-comment">
                                <h3 class="skills">Post Comments</h3>
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <input type="text" placeholder="Name" class="col-lg-12 form-control">
                                        </div>

                                        <div class="col-lg-4">
                                            <input type="text" placeholder="Email" class="col-lg-12 form-control">
                                        </div>

                                        <div class="col-lg-4">
                                            <input type="text" placeholder="2*five=? *" class="col-lg-12 form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <textarea placeholder="Message" rows="8" class=" form-control"></textarea>
                                        </div>
                                    </div>
                                    <p>
                                        <button type="submit" class="btn btn-danger pull-right">Post Comment</button>
                                    </p>
                                </form>
                            </div>

                        </div>   
                    </div>
                    <div class="row">

                    </div>
                </div>

            </div>

            <div class="col-lg-3">
                <div class="blog-side-item">
                    <div class="search-row">
                        <input type="text" class="form-control" placeholder="Search here">
                    </div>
                    <div class="category">
                        <h3>Categories</h3>
                        <ul class="list-unstyled">
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> Animals</a></li>
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> Landscape</a></li>
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> Portait</a></li>
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> Wild Life</a></li>
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> Video</a></li>
                        </ul>
                    </div>

                    <div class="blog-post">
                        <h3>Latest Blog Post</h3>
                        <div class="media">
                            <a class="pull-left" href="javascript:;">
                                <img class=" " src="{{url('img/blog/blog-thumb-1.jpg')}}" alt="">
                            </a>
                            <div class="media-body">
                                <h5 class="media-heading"><a href="javascript:;">02 May 2013 </a></h5>
                                <p>
                                    Donec id elit non mi porta gravida at eget metus amet int
                                </p>
                            </div>
                        </div>
                        <div class="media">
                            <a class="pull-left" href="javascript:;">
                                <img class=" " src="{{url('img/blog/blog-thumb-2.jpg')}}" alt="">
                            </a>
                            <div class="media-body">
                                <h5 class="media-heading"><a href="javascript:;">02 May 2013 </a></h5>
                                <p>
                                    Donec id elit non mi porta gravida at eget metus amet int
                                </p>
                            </div>
                        </div>
                        <div class="media">
                            <a class="pull-left" href="javascript:;">
                                <img class=" " src="{{url('img/blog/blog-thumb-3.jpg')}}" alt="">
                            </a>
                            <div class="media-body">
                                <h5 class="media-heading"><a href="javascript:;">02 May 2013 </a></h5>
                                <p>
                                    Donec id elit non mi porta gravida at eget metus amet int
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="tags">
                        <h3>Tags</h3>
                        <ul class="list-unstyled tag">
                            <li><a href="#">flat</a></li>
                            <li><a href="#"> clean</a></li>
                            <li><a href="#"> admin</a></li>
                            <li><a href="#"> UI</a></li>
                            <li><a href="#"> responsive</a></li>
                            <li><a href="#"> Web Design</a></li>
                            <li><a href="#"> UIX</a></li>
                            <li><a href="#"> Blog</a></li>
                            <li><a href="#">flat Admin</a></li>
                            <li><a href="#"> Dashboard</a></li>
                        </ul>
                    </div>


                    <div class="archive">
                        <h3>Archive</h3>
                        <ul class="list-unstyled">
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> May 2013</a></li>
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> April 2013</a></li>
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> March 2013</a></li>
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> February 2013</a></li>
                            <li><a href="javascript:;"><i class="  icon-angle-right"></i> January 2013</a></li>
                        </ul>
                    </div>


                </div>
            </div>

            <!--blog end-->
</div>
</div>
@stop